import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import addToMailchimp from 'gatsby-plugin-mailchimp';
import './subscribe.css';

export default class SubscribeComponent extends React.Component {

  state = {
    email: null
  }

  _handleChange = e => {
    this.setState({
      [`${e.target.name}`]: e.target.value
    })
  }

  _handleSubmit = e => {
    e.preventDefault()
    addToMailchimp(this.state.email)
      .then(({ msg, result }) => {
        if (result !== 'success') {
          throw msg
        }

        alert(msg)
      })
      .catch((err) => {
        console.log('err:', err)
        alert(err)
      })
  }

  render() {
    return (
    <div class="container subscribe">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <form onSubmit={this._handleSubmit}i>
            <p>
              <small>Keep me updated. Don't worry, no spam. Ever.</small>
            </p>
            <div class="form-group">
              <input type="email" class="form-control form-control-md" onChange={this._handleChange} placeholder="Your Email Address" name="email" />
              <input type="submit" class="btn btn-md" value="Subscribe!" />
            </div>
          </form>
        </div>
      </div>
    </div>
    )
  }

}
