import React from 'react'
import { Link } from "gatsby"
import { Container, Row, Col } from 'reactstrap';
import imgRuben from "../images/ruben.jpg";
import './bioRubenHome.css';

const BioRuben = () => (
  <div className="bio bio-home" id="section-bio">
    <Container>
      <Row>
        <Col sm="4">
          <img src={imgRuben} className="bio-profile-photo has-border-radius" alt="Ruben Mas, founder of Get Help Australia"/>
        </Col>
        <Col sm="8">
          <div className="bio-container">
            <h1 className="bio-title">Ruben Mas</h1>
            <h2 className="bio-subtitle">Founder and Head Consultant for Get Help Australia</h2>
            <ul>
              <li>Lost both parents before the age of 10</li>
              <li>8 years in maximum security jail</li>
              <li>Severe suicide attempt whilst in jail</li>
              <li>Hospitalised on 9 separate occasions over a period of 3 years with psychotic episodes due to drug addiction (Cocaine & Ice)</li>
              <li>Suffered from mental health issues</li>
              <li>Alone and homeless as family and friends lost hope</li>
              <li>It was at this point that Ruben decided to take responsibility, and his 3 year journey in rehab began.</li>
              <li>Today Ruben has developed a very successful career in sales and is repeatedly head hunted by some of Australia’s leading companies</li>
              <li>Founder of Get Help Australia</li>
            </ul>
            <div className="btn-wrapper">
              <Link to="/bio" className="btn btn-white btn-lg"> Read More </Link>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)

export default BioRuben
