// import React from 'react';
import React from "react";
import slide1 from '../images/slide1.png';
import slide2 from '../images/slide2.png';
// import placeholderImg3 from '../images/placeholder-3.jpg';
import { UncontrolledCarousel } from 'reactstrap';
import './carousel.css'

const items = [
  {
    // src: 'data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15ba800aa20%20text%20%7B%20fill%3A%23444%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15ba800aa20%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23666%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22247.3203125%22%20y%3D%22218.3%22%3ESecond%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E',
    src: slide1,
    // altText: 'Difficult Roads lead to Beautiful Destinations',
    caption: "We've been there so we understand",
    header: "Ask for help today"
  },
  {
    src: slide2,
    // altText: 'Ask for help today',
    caption: '',
    header: 'Difficult Roads often lead to Beautiful Destinations'
  },
  // {
  //   src: placeholderImg3,
  //   altText: 'Slide 3',
  //   // caption: 'Slide 3',
  //   header: 'Find your True-Self'
  // }
];

const HomeCarousel = () => (
  <div>
    <UncontrolledCarousel items={items} />
    <svg className="diagonal" viewBox="0 0 100 100" preserveAspectRatio="none">
      <polygon fill="#01579B" points="0,100 100,0 100,100"></polygon>
    </svg>
  </div>
)

export default HomeCarousel;