import React from 'react'
import { Container, Row, Col } from 'reactstrap';
import imgRuben from "../images/ruben2.jpg";
import './bioRuben.css'

const BioRuben = () => (
  <div className="bio" id="section-bio">
    <Container>
      <Row>
        <Col sm="5">
          <img src={imgRuben} className="bio-profile-photo circle" alt="Ruben Mas, founder of Get Help Australia"/>
          <p className="quote hide-xs"><i>"I keep meeting people who are in a lot pain and are suffering from issues which I have experienced many times in my life. When these people hear my story, they want to work with me. Formal education plays a role in rehabilitation, but in my experience, people feel more comfortable opening up to someone who has felt their pain. So together with my best friend Gonzalo Cal, we decided to create Get Help Australia. Our goal is to recruit people who have been through darkness, and are now ready to serve those who are ready to do the work."</i></p>
        </Col>
        <Col sm="7">
          <div className="bio-container">
            <h1 className="bio-title">Ruben Mas</h1>
            <h2 className="bio-subtitle">Founder and Head Consultant for Get Help Australia.</h2>
            <p className="intro">They say that strength comes from overcoming adversity, and Ruben has had his share of adversity throughout life. This is what has prepared him to be such a great mentor to people who struggle with addiction and trauma.</p>
            <p>At the age of 12, Ruben and his older sister arrived in Australia to live with their Uncle and Auntie. After losing their mother to illness and their father to suicide, life with their family in Australia was the only option.</p>
            <p>Not long after celebrating his 22nd  birthday, Ruben and his friends were sentenced to 8 years in jail for importation of drugs. The harshness of this reality was too much for a young Ruben to live with, and he tried to take his own life. As luck would have it, or perhaps it was divine intervention, he was found by a cellmate just in time.</p>
            <p>There was never a shortage of drugs in jail, but at this point Ruben wasn’t tempted. Once he was released from prison, Ruben felt like he had to catch up for many lost years. Excessive partying and a lack of positive role models would eventually lead to drugs, and it wasn't long before this developed into an addiction and a series of psychotic episodes, which would land him in hospital many times. The struggle with addiction went on for 3 years. It was a chaotic time. Friends and family were frustrated, lost all hope and they felt the only option was tough love. Ruben was now homeless, and it was at this point that he made the decision to enrol in rehab. First he spent some time at GROW Community Residential Rehab, before moving onto Odyssey House. This is known as one of Australia’s toughest rehabilitation programs, where only 2% of clients manage to make it to the end. Ruben managed to not only complete the program, but also spent extra time helping others with their addiction.           </p>
            <p>He was repeatedly told by some people that he would always be a useless loser who would never amount to anything. Ruben has now developed a very successful career in sales, working with some of Australia’s leading companies. However, his true passion is helping people. He spends a lot of his time helping friends who struggle with substance addiction, depression and trauma. He has also offered guidance to people who need help transitioning from jail to civilian life. Ruben has been told many times that if he really wants to officially help people, he must first complete long university degrees or be part of associations.</p>
            <p>There is no shortage of people who need help in the world, and for some reason a lot of these people gravitate towards Ruben for guidance. Maybe it’s because people who are in pain, recognise others who have been in pain.</p>
            <p className="show-xs"><i>"I keep meeting people who are in a lot pain and are suffering from issues which I have experienced many times in my life. When these people hear my story, they want to work with me. Formal education plays a role in rehabilitation, but in my experience, people feel more comfortable opening up to someone who has felt their pain. So together with my best friend Gonzalo Cal, we decided to create Get Help Australia. Our goal is to recruit people who have been through darkness, and are now ready to serve those who are ready to do the work."</i></p>
            <a className="btn btn-cta" href="tel:0426794453" rel="noopener noreferrer"><strong>Free Helpline: </strong><span className="inline-block">0426-794-453</span></a>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)

export default BioRuben
