import React from 'react'
import { Container, Row, Col } from 'reactstrap';
import './about.css'

const About = () => (
  <div className="about" id="section-about">
    <Container>
      <Row>
        <Col>
          <div className="about-container">
            <h3 className="about-title">We've been There.</h3>
            <p className="intro">Get Help Australia is a group of consultants <strong>with real life experience</strong>.</p>
            <p className="intro">Time and time again we see people trying to get help from professionals with only an academic background. Whilst their efforts and intentions are commendable, we find that it’s just not the same as receiving advice from someone who has actually been through darkness themselves, experienced extreme desperation and have overcome it.</p>
            <a className="btn btn-cta" href="tel:0426794453" rel="noopener noreferrer"><strong>Free Helpline: </strong><span className="inline-block">0426-794-453</span></a>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)

export default About
