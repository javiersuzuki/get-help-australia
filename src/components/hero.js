import React from 'react';
// import { Link } from 'gatsby';
import { Container, Row, Col } from 'reactstrap';
import Scrollchor from 'react-scrollchor';
// import Particles from 'react-particles-js';
import './hero.css';
import Subscribe from './subscribe.js'

const Hero = () => (
  <div className="hero">
  {/* <Particles
    className="Particles mask"
    params={
      {
        "particles": {
          "number": {
            "value": 280,
            "density": {
              "enable": true,
              "value_area": 950
            }
          },
          "color": {
            "value": "#fff"
          },
          "shape": {
            "type": "circle",
            "stroke": {
              "width": 1.5,
              "color": "#fff"
            }
          }
        }
      }
    }
  /> */}
    <Container>
      <Row>
        <Col>
          <div className="hero-container text-center">
            <h1 className="hero-title">Get Help Australia</h1>
            <h2 className="hero-subtitle">Consultancy helpline &mdash; talk to<br/>people with Real Life Experience</h2>
            <a class="btn btn-lg" href="tel:0426794453" rel="noopener noreferrer">Free Helpline: 0426-794-453</a>
          </div>
        </Col>
      </Row>
      <Subscribe />
    </Container>
    <svg className="diagonal" viewBox="0 0 100 100" preserveAspectRatio="none">
      <polygon fill="#fff" points="0,100 100,0 100,100"></polygon>
    </svg>
  </div>
)

export default Hero
