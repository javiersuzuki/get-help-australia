import React from 'react'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink } from 'reactstrap';
import Scrollchor from 'react-scrollchor';
import './topnav.css'

export default class TopnavHome extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div className="topnav home">
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">
            <svg className="logo-gha" viewBox="0 0 136 136"><g data-name="Layer 2"><path d="M101 60V40a2 2 0 0 0-2-2H87a2 2 0 0 0-2 2v20H51V40a2 2 0 0 0-2-2H37a2 2 0 0 0-2 2v56a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V76h34v20a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V76h18.39a52 52 0 0 1-99.31 12.24A52 52 0 0 1 115.14 46a2 2 0 0 0 2.58 1l11.06-4.68a2 2 0 0 0 1.05-2.65A68.14 68.14 0 1 0 136 68v-6a2 2 0 0 0-2-2z" data-name="Layer 1"/></g></svg>
          </NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>

                <NavItem className="nav-item--why-us">
                  <Scrollchor className="nav-link" to="#section-about">Why Us</Scrollchor>
                </NavItem>

                <NavItem className="nav-item--bio">
                  <NavLink href="/bio">About GHA</NavLink>
                </NavItem>

                {/* <NavItem>
                  <NavLink href="/podcast/">Podcast</NavLink>
                </NavItem> */}
                <NavItem className="">
                  <NavLink className="btn btn-gha" href="tel:0426794453" rel="noopener noreferrer"><strong>Free Helpline:</strong> <span className="phone-number">0426-794-453</span></NavLink>
                </NavItem>
              </Nav>
            </Collapse>
        </Navbar>
          {/* <a href="tel:1300-551-611" rel="noopener noreferrer"><strong>Free Advice Line:</strong> <span className="phone-number">1300 551 611</span></a> */}
          {/* <h1 className="topnav-title">
            <Link to="/" style={{ color: 'white', textDecoration: 'none' }}> {siteTitle} </Link>
          </h1> */}
          {/* <nav className="topnav-nav"></nav> */}
      </div>
    );
  }
}
