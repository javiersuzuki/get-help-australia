import React from 'react'
import { Container, Row, Col } from 'reactstrap';
import './footer.css'

const Footer = () => (
  <div className="footer" >
    <Container>
      <Row>
        <Col>
          <div className="footer-container">
            <p>© 2019 Get Help Australia . All Rights Reserved.</p>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
)

export default Footer
