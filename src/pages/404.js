import React from 'react'
import Layout from '../components/layout'

const NotFoundPage = () => (
  <Layout>
    <h1>Page Not Found</h1>
    <p>Feel free to give us a call <a className="" href="tel:1300-551-611"><strong>(Free Advice Line):</strong> 1300 551 611</a></p>
  </Layout>
)

export default NotFoundPage
