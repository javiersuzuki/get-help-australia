import React from 'react';
import Layout from '../components/layout';
import TopnavHome from '../components/topnavHome';
import HomeCarousel from '../components/carousel';
import Hero from '../components/hero';
import About from '../components/about';
import BioRubenHome from '../components/bioRubenHome';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

const IndexPage = () => (
  <Layout>
    <TopnavHome />
    <HomeCarousel />
    <Hero />
    <About />
    <BioRubenHome />
  </Layout>
)

export default IndexPage;
