import React from 'react';
import Layout from '../components/layout';
import Topnav from '../components/topnav';
import BioRuben from '../components/bioRuben'
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import './bio.css';

const IndexPage = () => (
  <Layout>
    <Topnav />
    <BioRuben />
  </Layout>
)

export default IndexPage;
