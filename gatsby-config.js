module.exports = {
  siteMetadata: {
    title: 'Get Help Australia',
    siteUrl: 'https://www.gethelpaustralia.com',
    description: 'Get Help Australia is a Drug and Alcohol Addiction Consultancy with Successful Real Life Experience. Free Advice Line: 1300-551-611',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Get Help Australia',
        short_name: 'Get Help AU',
        start_url: '/',
        background_color: '#03A9F4',
        theme_color: '#03A9F4',
        display: 'minimal-ui',
        icon: 'src/images/gatsby-icon.png', // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-128571939-1",
        head: true,
      },
    },
    'gatsby-plugin-offline',
    {
      resolve: 'gatsby-plugin-mailchimp',
      options: {
        endpoint: 'https://gethelpaustralia.us3.list-manage.com/subscribe/post?u=2bb7209272fd70a887d4f7755&amp;id=246b33a52d'
      }
    }
  ],
}
